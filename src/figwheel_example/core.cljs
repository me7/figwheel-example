(ns figwheel-example.core
  (:require [reagent.core :as r]
            [reagent.format :refer [format]]))

(enable-console-print!)


;simple
(defn simple-component []
  [:div
   [:p "I am a component"]
   [:p.someclass
    "I have " [:strong "bold"]
    [:span {:style {:color "red"}} " and red "] "text."]])

(defn simple-parent []
  [:div
   [:p "I include simple-component."]
   [simple-component]])


;with parameters
(defn hello-component [name]
  [:p "Hello, " name "!"])

(defn say-hello []
  [hello-component "ball"])


;loop
(defn lister [items]
  [:ul
   (for [i items]
     ^{:key i} [:li "Item " i])]) ;:key is for warning "seq element should have unique key"

(defn lister-user []
  [:div
   "Here is a list:"
   [lister (range 3)]])


;persistance state using "atom"
(def click-count (r/atom 0))
(defn counting-component []
  [:div
   "The atom " [:code "click-count"] "has value: "
   @click-count ". "
   [:input {:type "button" :value "Click me!"
            :on-click #(swap! click-count inc)}]])


;locally persistance state using "let"
(defn timer-component []
  (let [elapsed (r/atom 0)]
    (fn []
      (js/setTimeout #(swap! elapsed inc) 1000)
      [:div
       "Second Elapsed: " @elapsed])))


;share persistance state
(defn atom-input [val]
  [:input {:type "text" :value @val
           :on-change #(reset! val (-> % .-target .-value))}])
(defn shared-state []
  (let [val (r/atom "foo")]
    (fn []
      [:div
       [:p "The value is now: " @val]
       [:p "Change it here: " [atom-input val]]])))


;bmi component demo
(def bmi-data (r/atom {:height 167 :weight 74}))

(defn calc-bmi []
  (let [{:keys [height weight bmi] :as data} @bmi-data
        h (/ height 100)]
    (if (nil? bmi)
      (assoc data :bmi (/ weight (* h h)))
      (assoc data :weight (* bmi h h)))))

(defn slider [param value min max]
  [:input {:type "range" :value value :min min :max max
           :step "0.5" :style {:width "100%"}
           :on-change (fn [e]
                        (swap! bmi-data assoc param (.-target.value e))
                        (when (not= param :bmi)
                          (swap! bmi-data assoc :bmi nil)))}])

(defn bmi-component []
  (let [{:keys [height weight bmi]} (calc-bmi)
        [color diagnose] (cond
                           (< bmi 18.5) ["orange" "underweight"]
                           (< bmi 25) ["inherit" "normal"]
                           (< bmi 30) ["orange" "overweight"]
                           :else ["red" "obese"])]
    [:div
      [:h3 "BMI Calculator"]
      [:div
       "Height: " (format "%.1f" height) " cm"
         [slider :height height 100 220]]
      [:div
       "Weight " (format "%.1f" weight) " kg"
         [slider :weight weight 30 150]]
      [:div
         "BMI " (format "%.1f" bmi)
         [:div {:style {:color color}} diagnose]
         [slider :bmi bmi 10 50]]]))



;Clock example
(defonce timer (r/atom (js/Date.)))

(defonce time-updater
  (js/setInterval #(reset! timer (js/Date.)) 1000))

(defonce time-color (r/atom "#f34"))

(defn greeting [message]
  [:h1 message])

(defn clock []
  (let [time-str (-> @timer .toTimeString (clojure.string/split " ") first)]
    [:div.example-clock {:style {:color @time-color}} time-str]))

(defn color-input []
  [:div.color-input
   "Time color: "
   [:input {:type "text" :value @time-color
            :on-change #(reset! time-color (-> % .-target .-value))}]])

(defn clock-example []
  [:div
   [greeting "Hello world, it's now"]
   [clock]
   [color-input]])


(defn all-component []
  [:div
   ;[simple-parent]
   ;[lister-user]
   ;[counting-component]
   ;[timer-component]
   ;[shared-state]
   ;[bmi-component]
   [clock-example]])

;figwheel hook
(defn on-js-reload [])

;render-component is the main function
(r/render-component [all-component] (.getElementById js/document "app"))

