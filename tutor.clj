;======================SETUP=================================
;create bitbucket repo
https://bitbucket.org/repo/create

;clone to local
git clone git@bitbucket.ort:gampolt/figwheel-example.git

;create figwheel template
lein new figwheel figwheel-example --force

;upload first version
cd figwheel-example
git add --all
git commit -m "first commit, add tutor.clj"
git push origin master

;run fist version, then browse to localhost:3349
lein figwheel

;command line will show "cljs.user=>" you can try this
(js/alert "Hello from figwheel")

;Open console (Ctrl+Shift+J) you will see some text from core.cljs. try change it
(println "Edits to this text should show up in your developer console.")

;add reagent
[reagent "0.6.0-alpha"] ;in project.clj > :dependencies.
[reagent/core :as r] ;in core.cljs > (ns :require)
;after this you need to re-start figwheel
(defn hello-comp [] [:div "hello world"]) ; create component
;use render-component (component, div) function on div.app of index.html
(r/render-component [hello-comp] (.getElementById js/document "app"))
;if you want to add reagent from beginning, try "lein new figwheel PROJ -- --reagent"

;various of component sample, include BMI demo. Also sample use of reagent-utils
(defn all-component []
  [:div
   [simple-parent]
   [lister-user]
   [counting-component]
   [timer-component]
   [shared-state]
   [bmi-component]])


;now finish component example, let make it as a branch and upload
git checkout -b bmi-example
git commit -a -m "text for this branch"
git push --all -u

;see git email
git config user.email
;change for this project
git config user.email "gampolt@gmail.com"
;change it globally
git config --global user.email "gampolt@gmail.com"
